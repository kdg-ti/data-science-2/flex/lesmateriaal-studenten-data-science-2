import numpy as np
from matplotlib import pyplot as plt
from scipy import stats
from scipy.stats import chi2


def plot_two_way_t(p=0.95, n=30):
    t_min, t_max = -4, 4  # grenzen van de plot
    fig, ax = plt.subplots(figsize=(7, 3))
    α = 1 - p
    tα1 = stats.t.ppf(q=α / 2, df=n - 1)
    tα2 = stats.t.ppf(q=1 - α / 2, df=n - 1)
    t1 = np.linspace(t_min, t_max, num=1000)
    t2 = np.linspace(tα1, tα2, num=100)
    t3 = np.linspace(t_min, tα1, num=100)
    t4 = np.linspace(tα2, t_max, num=100)
    ax.plot(t1, stats.t.pdf(t1, df=n - 1))
    ax.vlines(tα1, ymin=0, ymax=stats.t.pdf(tα1, df=n - 1), color='green', linestyle='--')
    ax.vlines(tα2, ymin=0, ymax=stats.t.pdf(tα2, df=n - 1), color='green', linestyle='--')
    ax.set_ylim(0, 0.4)
    ax.fill_between(t2, stats.t.pdf(t2, df=n - 1), color='green', alpha=0.5, label='95% aanvaardingsinterval')
    ax.fill_between(t3, stats.t.pdf(t3, df=n - 1), color='red', alpha=0.5, label='α')
    ax.fill_between(t4, stats.t.pdf(t4, df=n - 1), color='red', alpha=0.5)
    ax.annotate('95%', xy=(0, 0.25), xytext=(-3, 0.3), arrowprops=dict(arrowstyle="->"))
    ax.annotate('α/2', xy=(-3, 0.005), xytext=(-3, 0.05), arrowprops=dict(arrowstyle="->"))
    ax.annotate('α/2', xy=(3, 0.005), xytext=(3, 0.05), arrowprops=dict(arrowstyle="->"))
    ax2 = ax.twiny()
    ax2.set_xlim(ax.get_xlim())
    ax2.set_xticks([tα1, tα2])
    ax2.set_xticklabels(['tα1', 'tα2'])
    ax2.tick_params(axis='x', pad=-210)
    _ = ax.legend()


def plot_chi_square(p=0.95, k=5):
    a = 1 - p
    x = chi2.ppf(1 - a, df=k - 1)
    fig, ax = plt.subplots(1, 1, figsize=(7, 3))
    x1 = np.linspace(0, 12, 500)
    x2 = np.linspace(0, x, 500)
    ax.plot(x1, chi2.pdf(x1, df=k - 1), label='df=k - 1')
    ax.fill_between(x2, chi2.pdf(x2, df=k - 1), color='green', alpha=0.3, edgecolor='black',
                    label='(1-α)% aanvaardingsinterval')
    ax.annotate('(1-α)%', xy=(4, 0.1), xytext=(4, 0.15), arrowprops=dict(arrowstyle="->"))
    ax.annotate(f'grenswaarde', xy=(x, 0), xytext=(x, 0.05), arrowprops=dict(arrowstyle="->"))
    ax.grid(linestyle='--', axis='y')
    ax.set_xlabel('χ²-waarde')
    ax.set_ylabel('kansdichtheid')
    ax.set_title('χ²-verdeling')
    ax.set_xlim(0, 12)
    _ = ax.legend()
