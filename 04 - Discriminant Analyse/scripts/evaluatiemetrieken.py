import numpy as np
import pandas as pd
from sklearn.metrics import roc_curve, roc_auc_score
import matplotlib.pyplot as plt


def accuracy(confusion_matrix: pd.DataFrame):
    return pd.DataFrame([np.sum(np.diag(confusion_matrix)) / confusion_matrix.to_numpy().sum()], index=['Total'],
                        columns=['Accuracy'])


def precision(confusion_matrix: pd.DataFrame, weighted=True):
    precisions = pd.DataFrame(np.diag(confusion_matrix) / confusion_matrix.sum(axis=1), columns=['Precision'])
    if weighted:
        weights = confusion_matrix.sum(axis=1)
        return pd.DataFrame([np.average(precisions, weights=weights, axis=0)],
                            columns=precisions.columns, index=['Average'])
    return pd.DataFrame(precisions)


def recall(confusion_matrix: pd.DataFrame, weighted=True):
    recalls = pd.DataFrame(np.diag(confusion_matrix) / confusion_matrix.sum(axis=0), columns=['Recall'])
    if weighted:
        weights = confusion_matrix.sum(axis=0)
        return pd.DataFrame([np.average(recalls, weights=weights, axis=0)],
                            columns=recalls.columns, index=['Average'])
    return pd.DataFrame(recalls)


def fmeasure(confusion_matrix: pd.DataFrame, beta=1, weighted=True):
    precisions = precision(confusion_matrix, weighted=False)
    recalls = recall(confusion_matrix, weighted=False)
    fs = pd.DataFrame(list(map(fvalue, precisions.values, recalls.values, np.full(len(precisions), beta))),
                      index=precisions.index, columns=['F{}'.format(beta)])
    if weighted:
        weights = confusion_matrix.sum(axis=1)
        return pd.DataFrame([np.average(fs, weights=weights, axis=0)], columns=fs.columns, index=['Average'])

    return fs


def fvalue(p, r, beta):
    return ((beta ** 2 + 1) * p * r) / (beta ** 2 * p + r)


def plot_roc(y_true, y_score, title='ROC Curve', **kwargs):
    if 'pos_label' in kwargs:
        fpr, tpr, thresholds = roc_curve(y_true=y_true, y_score=y_score, pos_label=kwargs.get('pos_label'))
        auc = 1 - roc_auc_score(y_true, y_score)
    else:
        fpr, tpr, thresholds = roc_curve(y_true=y_true, y_score=y_score)
        auc = roc_auc_score(y_true, y_score)

    optimal_idx = np.argmax(tpr - fpr)
    optimal_threshold = thresholds[optimal_idx]

    figsize = kwargs.get('figsize', (7, 7))
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    ax.grid(linestyle='--')

    # plot ROC curve
    ax.plot(fpr, tpr, color='darkorange', label='AUC: {:.3f}'.format(auc))
    ax.set_title(title)
    ax.set_xlabel('False Positive Rate (FPR)')
    ax.set_ylabel('True Positive Rate (TPR)')
    ax.fill_between(fpr, tpr, alpha=0.3, color='darkorange', edgecolor='black')

    # plot random classifier
    ax.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

    # plot optimal threshold
    ax.scatter(fpr[optimal_idx], tpr[optimal_idx],
               label='optimal cutoff {:.2f} op ({:.2f},{:.2f})'.format(optimal_threshold, fpr[optimal_idx],
                                                                       tpr[optimal_idx]), color='red')
    ax.plot([fpr[optimal_idx], fpr[optimal_idx]], [0, tpr[optimal_idx]], linestyle='--', color='red')
    ax.plot([0, fpr[optimal_idx]], [tpr[optimal_idx], tpr[optimal_idx]], linestyle='--', color='red')

    ax.legend(loc='lower right')
    plt.show()


if __name__ == '__main__':
    confusion_matrix = pd.DataFrame({'NO': [50, 10], 'YES': [5, 100]}, index=['NO', 'YES'])
    print(precision(confusion_matrix))
    print(precision(confusion_matrix, False))

    print(recall(confusion_matrix))
    print(recall(confusion_matrix, False))

    print(fmeasure(confusion_matrix))
    print(fmeasure(confusion_matrix, beta=1, weighted=False))
