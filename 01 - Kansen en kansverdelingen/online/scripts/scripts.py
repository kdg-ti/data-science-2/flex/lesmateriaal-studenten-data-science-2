import numpy as np
from venn import venn


def plot_venn(lists, labels, **kwargs):
    """Plot a Venn diagram of multiple lists.
    """

    sets = dict(zip(labels, [set(zip(range(1, len(l) + 1), l)) for l in lists]))

    _ = venn(sets, **kwargs)


def highlight_background(x, color):
    return np.where(x == x, f"background: {color};", None)


def highlight_cell(col, col_label, row_label, color='yellow'):
    # check if col is a column we want to highlight
    if col.name == col_label:
        # a boolean mask where True represents a row we want to highlight
        mask = (col.index == row_label)
        # return an array of string styles (e.g. ["", "background-color: yellow"])
        return [f"color: {color}" if val_bool else "" for val_bool in mask]
    else:
        # return an array of empty strings that has the same size as col (e.g. ["",""])
        return np.full_like(col, "", dtype="str")


def highlight_row(row, row_label, color='yellow'):
    # a boolean mask where True represents a row we want to highlight
    mask = (row.index == row_label)
    # return an array of string styles (e.g. ["", "background-color: yellow"])
    return [f"background-color: {color}" if val_bool else "" for val_bool in mask]


def highlight_col(col, col_label, color='yellow'):
    # check if col is a column we want to highlight
    if col.name == col_label:
        # return an array of string styles (e.g. ["", "background-color: yellow"])
        return [f"background-color: {color}" for _ in col]
    else:
        # return an array of empty strings that has the same size as col (e.g. ["",""])
        return np.full_like(col, "", dtype="str")
