# Oefenmateriaal voor Data Science 2

Dit is de repository voor het oefenmateriaal voor het vak Data Science 2.

* Onder **oefeningen** vind je notebooks met de **opgaven**, images en **datasets**.
* Onder de **slides** folder vind je notebooks die aansluiten bij de slides. Je kan deze gebruiken om de leerstof in te
  oefenen. Soms zijn daar ook de **datasets** bij die aan bod komen in de slides. Je vindt er ook de slides (op Canvas vind
  je echter steeds de meest recente versies).
* Onder **online** vind je het lesmateriaal dat gebruikt werd tijdens de online sessies.
